import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import "@/filters/filters"

Vue.config.productionTip = false

Vue.prototype.$filters = Vue.options.filters

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
