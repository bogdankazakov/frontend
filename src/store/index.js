import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    lang: 'ru'
  },
  getters: {
    LANGUAGE: state => {
      return state.lang;
    }
  },
  mutations: {
    changelang (state, n) {
      state.lang = n
    }
  },
  actions: {
  },
  modules: {
  }
})
