const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
    ? '/app/'
    : '/app/',
    outputDir: '../fins/app_frontend/',
    assetsDir: '../static/',
    indexPath: './templates/app_frontend/index.html',
    configureWebpack:{
        optimization: {
            splitChunks: {
                chunks: 'all'
            },
        },
        plugins: [
            new BundleAnalyzerPlugin()
        ]
    },

};
